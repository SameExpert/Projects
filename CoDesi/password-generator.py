from math import floor
from random import randint,choice,shuffle

length = int(input("Enter the length of the password: "))

letters = "abcdefghijklmnopqrstuvwxyz"
symbols = "!@#$%^&*()_+[]{};:<>?,./\\|"

password = ""

for i in range(floor(length*0.5)):
    password += choice(letters)

for i in range(floor(length*0.3)):
    password += choice(symbols)

for i in range(floor(length*0.2)):
    password += str(randint(0,9))

while length != len(password):
    password += str(choice([choice(letters), choice(symbols), randint(0,9)]))

#password = shuffle(list(password))

print(password)
