from random import choice

words = ["keyboard", "mouse", "monitor", "speaker", "headphone"]

word = choice(words)

guess_list = []

_ = [" _ "] * len(word)

while "".join(_) != word:
    print(" ".join(_))
    guess = input("Guess a letter: ")
    for i in range(len(word)):
        if guess == word[i]: _[i] = guess
    
    guess_list.append(guess)
    print("You guessed these letters: " + str(guess_list))
