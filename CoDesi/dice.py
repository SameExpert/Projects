from random import randint

while True:
    dice = randint(1,6)
    guess = input("Guess the dice: ")
    if int(guess) == dice:
        print("You guessed it!")
        break
    else: print("Oops! The number was " + str(dice))

