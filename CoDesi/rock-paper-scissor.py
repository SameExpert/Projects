from random import randint, choice

rps = ["rock", "paper", "scissor"]
score_user = 0
score_comp = 0
count = 0
while count < 5:
    comp = randint(1,3)
    user = input("Enter \'r\' for " + rps[0] + ", \'p\' for " + rps[1] + " or \'s\' for " + rps[2] + ": ")
    
    if user == "r" or user == "R": user = 1
    elif user == "p" or user == "P": user = 2
    elif user == "s" or user == "S": user = 3
    else:
        print("Wrong input, try again.")
        continue
    
    count += 1
    print("You: ", rps[user-1])
    print("Computer: ", rps[comp-1])
    
    if user == comp+1 or (comp == 3 and user == 1):
        score_user += 1
        print("You won!!!")
    elif comp == user+1 or (user == 3 and comp == 1):
        score_comp += 1
        print("You lost!!!")
    else: print("That was a tie!!!")

print ("\n")
print("Your score: " + str(user))
print("Computer's score: " + str(comp))
if user > comp: print("Congrats, You won the series!")
elif user < comp: print("Better luck next time!")
else: print("You both won equally!!!")
