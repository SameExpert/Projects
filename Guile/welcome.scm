(define (greet name)
  (display
   (string-append "Welcome " name "!" "\n")))

(define items
 (list "Ice Cream" "Cookies" "Rava Idli" "Vada" "Dosa"))

(greet "Samantha")
(display "What do you want?\n")
(for-each
  (lambda (item)
    (display (string-append " * " item "\n")))
  items)
