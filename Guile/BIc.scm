(use-modules (ice-9 ftw))

(define wanted-files
  ;; note that these are the stand-ins for the full paths of the files;
  ;; you can see a different approach in the file-system-fold documentation:
  ;; https://www.gnu.org/software/guile/manual/html_node/File-Tree-Walk.html
  (list "some" "files" "you" "want"))

(define (no-op . args) #t)

(define unwanted-files
  ;; these no-ops are procedures we don't care about for present purposes.
  ;; you may want to replace the first of these with a procedure checking to
  ;; ensure you don't enter a symlinked directory or the like, depending on
  ;; your goals
  (file-system-fold no-op
                    (lambda (name stat result)
                      ;; this is the "leaf" procedure called on each directory
                      ;; we enter or file we encounter
                      (if (member name wanted-files)
                          result
                          (cons name result)))
                    no-op no-op no-op
                    (lambda (name stat errno result)
                      ;; error handling, taken straight from the example
                      (format (current-error-port) "warning: ~a: ~a%" name (strerr errno))
                      result)
                    '() (getcwd)))

(map delete-file unwanted-files)
